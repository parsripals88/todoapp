package com.todoapp.mapper;

import com.todoapp.command.Command;
import com.todoapp.command.ExitCommand;
import com.todoapp.command.ListCommand;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandMapperTest {

    CommandMapper commandMapper;

    @Before
    public void setUp() {
        commandMapper = new CommandMapper();
    }

    @Test
    public void mapStringToObjectNoArgsConstructor() throws Exception {
        Command command = (Command) commandMapper.mapStringToObject("exit");
        Assert.assertThat("Command mapper works", command, CoreMatchers.instanceOf(ExitCommand.class));
    }

    @Test
    public void mapStringToObjectParameterConstructor() throws Exception {
        Command command = (Command) commandMapper.mapStringToObject("list all");
        Assert.assertThat("Command mapper works", command, CoreMatchers.instanceOf(ListCommand.class));
    }

    @Test
    public void mapStringToObjectNotPresent() throws Exception {
        Command command = (Command) commandMapper.mapStringToObject("find all");
        Assert.assertThat("Command mapper works", command, CoreMatchers.nullValue());
    }
}