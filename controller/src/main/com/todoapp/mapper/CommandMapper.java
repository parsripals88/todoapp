package com.todoapp.mapper;

import com.todoapp.annotation.CommandLineOption;
import com.todoapp.command.Command;
import com.todoapp.util.StringUtil;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.util.*;

public class CommandMapper implements Mapper {

    private static Map<String, Class> mapData = new HashMap<>();

    public CommandMapper() {
        if (mapData.size() == 0)
            loadCommandClasses();
    }

    /**
     * convert string to command class
     *
     * @param str
     * @return
     */
    @Override
    public Object mapStringToObject(String str) {
        String command = parseCommand(str);
        if (command != null) {
            try {
                if (mapData.containsKey(command)) {
                    Class cl = mapData.get(command);
                    Constructor[] constructors = cl.getConstructors();
                    if (constructors[0].getParameterCount() == 0) {
                        return cl.newInstance();
                    } else {
                        Constructor<?> cons = cl.getConstructor(String.class);
                        return cons.newInstance(parseCommandOption(str, command));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("System Error! Invalid Command " + str);
            }
        }

        System.out.println("Invalid Command " + str);
        return null;
    }

    private String parseCommandOption(String input, String command) {
        if (input.equals(command)) {
            return StringUtil.emptyString();
        } else {
            return input.substring(command.length() + 1);
        }

    }

    private String parseCommand(String string) {
        StringTokenizer tokenizer = new StringTokenizer(string);
        if (tokenizer.hasMoreElements()) {
            return (String) tokenizer.nextElement();
        } else {
            return null;
        }
    }

    private void loadCommandClasses() {
        Reflections reflections = new Reflections("com.todoapp.command");
        for (Class<?> cl : reflections.getTypesAnnotatedWith(CommandLineOption.class)) {
            CommandLineOption commandLineOption = cl.getAnnotation(CommandLineOption.class);
            mapData.put(commandLineOption.name(), cl);
        }
    }
}
