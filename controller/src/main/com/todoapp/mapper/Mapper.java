package com.todoapp.mapper;

public interface Mapper {

    public Object mapStringToObject(String string);
}
