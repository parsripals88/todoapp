package com.todoapp.database;

import com.todoapp.indexer.DescriptionIndexer;
import com.todoapp.indexer.Indexer;
import com.todoapp.indexer.TagIndexer;
import com.todoapp.model.Note;
import com.todoapp.model.Notes;

public class NotesInMemoryDAOImpl implements NotesDAO {

    private Notes notes;
    private static NotesDAO instance;
    Indexer tagIndexer = TagIndexer.getInstance();
    Indexer descIndexer = DescriptionIndexer.getInstance();

    private NotesInMemoryDAOImpl() {
        notes = new Notes();
    }

    public static NotesDAO getInstance() {
        if (instance == null) {
            instance = new NotesInMemoryDAOImpl();
        }
        return instance;
    }

    public static NotesDAO clearAndGetInstance() {
        instance = new NotesInMemoryDAOImpl();
        return instance;
    }

    @Override
    public Notes listAll() {
        return notes;
    }

    @Override
    public Note listNote(int id) {
        return notes.getNote(id);
    }

    @Override
    public boolean addNote(Note note) {
        int id = notes.getUniqueId();
        note.setId(id);
        tagIndexer.add(note);
        descIndexer.add(note);
        notes.addNote(note);
        return true;
    }

    @Override
    public boolean updateNote(Note note) {
        Integer id = note.getId();
        if (!listNote(id).isValid()) {
            System.out.println("Sorry! Note not found");
            return false;
        }
        tagIndexer.update(notes.getNote(id), note);
        descIndexer.update(notes.getNote(id), note);
        notes.deleteNote(id);
        notes.addNote(note);
        return true;
    }

    @Override
    public boolean deleteNote(int id) {
        tagIndexer.delete(notes.getNote(id));
        descIndexer.delete(notes.getNote(id));
        notes.deleteNote(id);
        return true;
    }

    @Override
    public boolean deleteAll() {
        tagIndexer.deleteAll();
        descIndexer.deleteAll();
        notes.clear();
        return true;
    }
}
