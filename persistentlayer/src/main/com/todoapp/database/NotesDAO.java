package com.todoapp.database;

import com.todoapp.model.Note;
import com.todoapp.model.Notes;

public interface NotesDAO {

    public Notes listAll();

    public Note listNote(int id);

    public boolean addNote(Note note);

    public boolean updateNote(Note note);

    public boolean deleteNote(int id);

    public boolean deleteAll();
}
