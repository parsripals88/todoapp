package com.todoapp.storage;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TrieNode {
    static final int SIZE = 128;

    TrieNode[] children = new TrieNode[SIZE];
    Set<Integer> ids;

    TrieNode() {
        ids = new HashSet<>();
        for (int i = 0; i < SIZE; i++)
            children[i] = null;
    }

    public void addId(int id) {
        ids.add(id);
    }

    public void removeId(int id) {
        ids.remove(id);
    }

    public Set<Integer> getIds() {
        return ids;
    }
}
