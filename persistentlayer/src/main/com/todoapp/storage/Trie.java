package com.todoapp.storage;

import java.util.LinkedList;
import java.util.List;

public class Trie {
    TrieNode root = new TrieNode();

    public void insert(String key, int id) {
        TrieNode current = createNotes(key);
        current.addId(id);
    }

    public void delete(String key, int id) {
        TrieNode current = createNotes(key);
        current.removeId(id);
    }

    public List<Integer> search(String key) {
        TrieNode current = createNotes(key);
        return new LinkedList<>(current.getIds());
    }

    public void clear() {
        root = new TrieNode();
    }

    private TrieNode createNotes(String key) {
        int length = key.length();
        TrieNode current = root;
        for (int level = 0; level < length; level++) {
            int index = key.charAt(level) - ' ';
            if (current.children[index] == null)
                current.children[index] = new TrieNode();

            current = current.children[index];
        }
        return current;
    }


}
