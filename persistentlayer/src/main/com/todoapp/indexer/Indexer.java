package com.todoapp.indexer;

import com.todoapp.model.Note;

import java.util.List;

public interface Indexer {

    public List<Integer> search(String word);

    public List<Integer> search(List<String> word);

    public void add(Note note);

    public void update(Note oldNote, Note newNote);

    public void delete(Note note);

    public void deleteAll();
}
