package com.todoapp.indexer;

import com.todoapp.model.Note;
import com.todoapp.util.ListUtil;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TagIndexer implements Indexer {

    Map<String, List<Integer>> tagIndex;
    private static Indexer instance;

    private TagIndexer() {
        tagIndex = new LinkedHashMap<>();
    }

    public static Indexer getInstance() {
        if (instance == null) {
            instance = new TagIndexer();
        }
        return instance;
    }

    @Override
    public List<Integer> search(String word) {
        if (tagIndex.containsKey(word)) {
            return tagIndex.get(word);
        }
        return null;
    }

    @Override
    public List<Integer> search(List<String> words) {
        List<Integer> masterList = new LinkedList<>();
        if (words == null || words.size() == 0)
            return masterList;
        for (String word : words) {
            if (tagIndex.containsKey(word)) {
                List<Integer> tempList = tagIndex.get(word);
                if (masterList.size() == 0) {
                    masterList = tempList;
                } else {
                    masterList = ListUtil.intersectList(masterList, tempList);
                }
            }
        }
        return masterList;
    }

    @Override
    public void add(Note note) {
        List<Integer> list;
        for (String tag : note.getTags()) {
            if (tagIndex.containsKey(tag)) {
                list = tagIndex.get(tag);
            } else {
                list = new LinkedList<>();
            }
            list.add(note.getId());
            tagIndex.put(tag, list);
        }
    }

    @Override
    public void update(Note oldNote, Note newNote) {
        delete(oldNote);
        add(newNote);
    }

    @Override
    public void delete(Note note) {
        List<Integer> list;
        for (String tag : note.getTags()) {
            list = tagIndex.get(tag);
            list.remove(note.getId());
            tagIndex.put(tag, list);
        }
    }

    @Override
    public void deleteAll() {
        tagIndex.clear();
    }
}
