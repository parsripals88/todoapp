package com.todoapp.indexer;

import com.todoapp.model.Note;
import com.todoapp.storage.Trie;
import com.todoapp.util.ListUtil;
import com.todoapp.util.StringUtil;

import java.util.LinkedList;
import java.util.List;

public class DescriptionIndexer implements Indexer {

    private static Indexer instance;
    private Trie trie;

    private DescriptionIndexer() {
        trie = new Trie();
    }

    public static Indexer getInstance() {
        if (instance == null) {
            instance = new DescriptionIndexer();
        }
        return instance;
    }

    @Override
    public List<Integer> search(String word) {
        return trie.search(word);
    }

    @Override
    public List<Integer> search(List<String> words) {
        List<Integer> masterList = new LinkedList<>();
        if (words == null || words.size() == 0)
            return masterList;
        for (String word : words) {
            List<Integer> tempList = search(word);
            if (masterList.size() == 0) {
                masterList = tempList;
            } else {
                masterList = ListUtil.intersectList(masterList, tempList);
            }
        }
        return masterList;
    }

    @Override
    public void add(Note note) {
        List<String> words = StringUtil.splitStringBySpace(note.getDescription());
        for (String word : words) {
            trie.insert(word, note.getId());
        }
    }

    @Override
    public void update(Note oldNote, Note newNote) {
        delete(oldNote);
        add(newNote);
    }

    @Override
    public void delete(Note note) {
        List<String> words = StringUtil.splitStringBySpace(note.getDescription());
        for (String word : words) {
            trie.delete(word, note.getId());
        }

    }

    @Override
    public void deleteAll() {
        trie.clear();
    }
}
