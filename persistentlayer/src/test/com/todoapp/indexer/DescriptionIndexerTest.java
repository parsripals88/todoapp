package com.todoapp.indexer;

import com.todoapp.model.Note;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DescriptionIndexerTest {

    Indexer descIndexer;
    Note note;

    @Before
    public void setUp() throws Exception {
        descIndexer = DescriptionIndexer.getInstance();
        note = new Note();
        note.setId(1);
        note.setDescription("alpha is awesome");
        descIndexer.add(note);
        note = new Note();
        note.setId(2);
        note.setDescription("coding is awesome");
        descIndexer.add(note);
    }

    @Test
    public void search() throws Exception {
        List<Integer> integer = descIndexer.search("is");
        Assert.assertThat("search", integer.size(), CoreMatchers.is(2));
    }

    @Test
    public void search1() throws Exception {
        List<Integer> integer = descIndexer.search(Arrays.asList("is", "awesome"));
        Assert.assertThat("search with many words", integer.size(), CoreMatchers.is(2));
    }

    @Test
    public void searchNull() throws Exception {
        List<String> words = null;
        List<Integer> integer = descIndexer.search(words);
        Assert.assertThat("search with many words", integer.size(), CoreMatchers.is(0));
    }

    @Test
    public void searchEmptyList() throws Exception {
        List<Integer> integer = descIndexer.search(new ArrayList<>());
        Assert.assertThat("search with many words", integer.size(), CoreMatchers.is(0));
    }

    @Test
    public void update() throws Exception {
        Note newNote = new Note();
        newNote.setId(2);
        newNote.setDescription("alpha is also there");
        descIndexer.update(note, newNote);
        List<Integer> integer = descIndexer.search(Arrays.asList("alpha"));
//        Assert.assertThat("update and search", integer, CoreMatchers.hasItem(2));
    }

    @Test
    public void delete() throws Exception {
        descIndexer.delete(note);
        List<Integer> integer = descIndexer.search(Arrays.asList("is"));
//        Assert.assertThat("update and search", integer, CoreMatchers.hasItem(1));
    }

    @Test
    public void deleteAll() throws Exception {
        descIndexer.deleteAll();
        List<Integer> integer = descIndexer.search(Arrays.asList("is"));
        Assert.assertThat("update and search", integer.size(), CoreMatchers.is(0));
    }

}