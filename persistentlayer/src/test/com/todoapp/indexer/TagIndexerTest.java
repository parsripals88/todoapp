package com.todoapp.indexer;

import com.todoapp.model.Note;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TagIndexerTest {

    Indexer tagIndexer;
    Note note;

    @Before
    public void setUp() throws Exception {
        tagIndexer = TagIndexer.getInstance();
        note = new Note();
        note.setId(1);
        note.setTags(Arrays.asList("alpha", "is", "there"));
        tagIndexer.add(note);
        note = new Note();
        note.setId(2);
        note.setTags(Arrays.asList("coding", "is", "awesome"));
        tagIndexer.add(note);
    }

    @Test
    public void search() throws Exception {
        List<Integer> integer = tagIndexer.search("is");
        Assert.assertThat("search", integer.size(), CoreMatchers.is(2));
    }

    @Test
    public void search1() throws Exception {
        List<Integer> integer = tagIndexer.search(Arrays.asList("is", "awesome"));
        Assert.assertThat("search with many words", integer.size(), CoreMatchers.is(2));
    }

    @Test
    public void searchNull() throws Exception {
        List<String> words = null;
        List<Integer> integer = tagIndexer.search(words);
        Assert.assertThat("search with many words", integer.size(), CoreMatchers.is(0));
    }

    @Test
    public void searchEmptyList() throws Exception {
        List<Integer> integer = tagIndexer.search(new ArrayList<>());
        Assert.assertThat("search with many words", integer.size(), CoreMatchers.is(0));
    }

    @Test
    public void update() throws Exception {
        Note newNote = new Note();
        newNote.setId(2);
        newNote.setTags(Arrays.asList("alpha", "is", "also", "there"));
        tagIndexer.update(note, newNote);
        List<Integer> integer = tagIndexer.search(Arrays.asList("alpha"));
//        Assert.assertThat("update and search", integer, CoreMatchers.hasItem(2));
    }

    @Test
    public void delete() throws Exception {
        tagIndexer.delete(note);
        List<Integer> integer = tagIndexer.search(Arrays.asList("is"));
//        Assert.assertThat("update and search", integer, CoreMatchers.hasItem(1));
    }

    @Test
    public void deleteAll() throws Exception {
        tagIndexer.deleteAll();
        List<Integer> integer = tagIndexer.search(Arrays.asList("is"));
        Assert.assertThat("update and search", integer.size(), CoreMatchers.is(0));
    }

}