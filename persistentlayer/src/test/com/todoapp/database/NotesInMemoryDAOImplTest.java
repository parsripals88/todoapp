package com.todoapp.database;

import com.todoapp.model.Note;
import com.todoapp.model.Notes;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NotesInMemoryDAOImplTest {

    NotesDAO dao;
    Note note;

    @Before
    public void setUp() throws Exception {
        dao = NotesInMemoryDAOImpl.clearAndGetInstance();
        note = new Note();
        note.setDescription("alpha is great");
        dao.addNote(note);
        note = new Note();
        note.setDescription("beta is great");
        dao.addNote(note);
    }

    @Test
    public void listAll() throws Exception {
        Notes notes = dao.listAll();
        Assert.assertThat("List all",
                notes.getSize(),
                CoreMatchers.is(2));
    }

    @Test
    public void listNote() throws Exception {
        Note note = dao.listNote(1);
        Assert.assertThat("List all",
                note.getDescription(),
                CoreMatchers.is("alpha is great"));
    }

    @Test
    public void updateNote() throws Exception {
        Note newNote = new Note();
        newNote.setId(2);
        newNote.setDescription("Gamma is great");
        dao.updateNote(newNote);
        Assert.assertThat("update note check",
                dao.listNote(2).getDescription(),
                CoreMatchers.is("Gamma is great"));
    }

    @Test
    public void updateNonExistingNote() throws Exception {
        Note newNote = new Note();
        newNote.setId(3);
        newNote.setDescription("Gamma is great");
        boolean bool = dao.updateNote(newNote);
        Assert.assertThat("update note check",
                bool,
                CoreMatchers.is(false));
        Assert.assertThat("update note check",
                dao.listNote(3).getDescription(),
                CoreMatchers.nullValue());
    }

    @Test
    public void deleteNote() throws Exception {
        dao.deleteNote(2);
        Assert.assertThat("delete note check",
                dao.listNote(2).getId(),
                CoreMatchers.nullValue());
    }

    @Test
    public void deleteAll() throws Exception {
        dao.deleteAll();
        Assert.assertThat("delete note check",
                dao.listAll().getSize(),
                CoreMatchers.is(0));
    }

}