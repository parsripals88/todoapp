**TODO APP**

Backend application for adding/updating/deleting TODO tasks.

*Google Keep Application*

---

## Command Line Options Supported

1. add content - command to add a todo note
2. update id content - command to update a todo note
3. list id - command to print note details
4. list all - command to print all the note details
5. searchtag tag - command to search tag in a todo note. Implemented using in-memory key-value pair index.
6. searchtag tags - command to search tags in a todo note. Implemented using in-memory key-value pair index.
7. searchdesc word - command to search for single word in a todo note. Implemented using trie.
8. searchdesc words - command to search for multiple words in a todo note. Implemented using trie.

---
## Limitations

1. Single user app. Can be expanded to multi-user with changes in the persistent layer.
2. Functionality to be added in future : Reminder, adding more formats (images, videos)
3. Colloborating and Sharing on the notes.
4. Pin, archive and mark as completed notes.
5. Using a persistent data store i.e. sql database.
---

## Modules 

Next, will explain the architecture of the app.

1. **driver** module : Contains the main program to trigger command line options.
2. **common** module : Contains all the common packages i.e. model, writer, enum, interface and annotations accessed by other modules.
3. **controller** module : contains the mapper to convert input string to corresponding command.
4. **readservice** module : contains all the read operation from the persistent layer
5. **writeservice** module : contains all the write operation to the persisten layer.
6. **persistentlayer** module : contains all the persistent component i.e. in memory database.
---

## Feature stories on the roadmap

1. Supporting concurrency.
2. Multiple user login.
3. Adding in request handlers.
4. Using kafka for building indexes.
5. Using redis as key-value pair index. 
6. Localization of text messages
