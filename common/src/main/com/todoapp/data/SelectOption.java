package com.todoapp.data;

public enum SelectOption {

    ALL,
    SINGLE,
    INVALID;

}
