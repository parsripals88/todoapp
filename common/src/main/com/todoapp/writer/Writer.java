package com.todoapp.writer;

public interface Writer {

    public void write(Object object);
}
