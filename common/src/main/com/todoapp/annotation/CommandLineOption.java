package com.todoapp.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CommandLineOption {

    /**
     * name of the command line option
     *
     * @return
     */
    String name();

    /**
     * description of the command line option
     *
     * @return
     */
    String description();


    /**
     * describes all the option available for command line
     *
     * @return
     */
    String options();
}
