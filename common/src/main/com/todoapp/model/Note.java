package com.todoapp.model;

import java.util.LinkedList;
import java.util.List;

public class Note {

    private Integer id;

    private String description;

    private List<String> tags = new LinkedList<>();

    public Note() {

    }

    public Note(int id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getTags() {
        return tags;
    }

    public void addTag(String tag) {
        this.tags.add(tag);
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isValid() {
        return id != null;
    }

    public String toString() {
        if (isValid()) {
            return isValidString();
        } else {
            return isInvalidString();
        }
    }

    private String isValidString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Note");
        builder.append(" ");
        builder.append(id);
        builder.append(" : ");
        builder.append(description);
        for (String tag : tags) {
            builder.append(" #");
            builder.append(tag);
        }
        return builder.toString();
    }

    private String isInvalidString() {
        return "No note present";
    }
}
