package com.todoapp.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Notes {

    Map<Integer, Note> notes = new LinkedHashMap<>();

    public void addNote(Note note) {
        notes.put(note.getId(), note);
    }

    public void deleteNote(int id) {
        notes.remove(id);
    }

    public Note getNote(int id) {
        if (notes.containsKey(id)) {
            return notes.get(id);
        } else {
            return new Note();
        }
    }

    public int getSize() {
        return notes.size();
    }

    public int getUniqueId() {
        return getSize() + 1;
    }

    public void clear() {
        notes.clear();
    }

    public boolean isEmpty() {
        return notes.size() == 0;
    }

    public String toString() {
        if (isEmpty()) {
            return emptyNoteString();
        } else {
            return nonEmptyNoteListToString();
        }
    }

    public String nonEmptyNoteListToString() {
        StringBuilder builder = new StringBuilder();
        for (Note note : notes.values()) {
            builder.append(note.toString());
            builder.append("\n");
        }
        builder.delete(builder.lastIndexOf("\n"), builder.length());
        return builder.toString();
    }

    public String emptyNoteString() {
        return "No notes present.";
    }
}
