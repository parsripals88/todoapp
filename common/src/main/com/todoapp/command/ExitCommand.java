package com.todoapp.command;

import com.todoapp.annotation.CommandLineOption;

@CommandLineOption(name = "exit", description = "exit the command line", options = "")
public class ExitCommand implements Command {

    @Override
    public void execute() {
        System.out.println("Program Terminated");
        System.exit(0);
    }
}
