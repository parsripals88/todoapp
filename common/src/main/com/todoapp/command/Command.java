package com.todoapp.command;

public interface Command {

    public void execute();
}
