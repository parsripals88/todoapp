package com.todoapp.command;

import com.todoapp.annotation.CommandLineOption;
import org.reflections.Reflections;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 */
@CommandLineOption(name = "help", description = "exit the command line", options = "")
public class HelpCommand implements Command {

    Set<String> commandText = new TreeSet<>();

    /**
     *
     */
    @Override
    public void execute() {
        populateCommandText();
        printCommandTextInSortedOrder();
    }

    /**
     *
     */
    private void printCommandTextInSortedOrder() {
        for (String str : commandText) {
            System.out.println(str);
        }
    }

    /**
     *
     */
    public void populateCommandText() {
        Reflections reflections = new Reflections("com.todoapp.command");
        for (Class<?> cl : reflections.getTypesAnnotatedWith(CommandLineOption.class)) {
            CommandLineOption commandLineOption = cl.getAnnotation(CommandLineOption.class);
            commandText.add(convertCommandLineOptionToString(commandLineOption));
        }
    }

    /**
     * @param commandLineOption
     * @return
     */
    private String convertCommandLineOptionToString(CommandLineOption commandLineOption) {
        StringBuilder builder = new StringBuilder(commandLineOption.name());
        builder.append(" ");
        builder.append(commandLineOption.options());
        builder.append(" - ");
        builder.append(commandLineOption.description());
        return builder.toString();
    }
}
