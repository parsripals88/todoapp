package com.todoapp.util;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class StringUtil {

    /**
     * split string by space
     *
     * @param input
     * @return
     */
    public static List<String> splitStringBySpace(String input) {
        List<String> words = new LinkedList<>();
        if (!isEmpty(input)) {
            StringTokenizer tokenizer = new StringTokenizer(input);
            while (tokenizer.hasMoreElements()) {
                String str = (String) tokenizer.nextElement();
                words.add(str);
            }
        }
        return words;
    }


    /**
     * checks for empty string
     *
     * @param s
     * @return
     */
    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }


    /**
     * @return an empty string
     */
    public static String emptyString() {
        return "";
    }
}
