package com.todoapp.util;

import java.util.LinkedList;
import java.util.List;

public class ListUtil {

    /**
     * creates the union of two integer list.
     */
    public static List<Integer> intersectList(List<Integer> firstList, List<Integer> secondList) {
        List<Integer> list = new LinkedList<>();
        for (Integer integer : firstList) {
            if (secondList.contains(integer)) {
                list.add(integer);
            }
        }
        return list;
    }
}
