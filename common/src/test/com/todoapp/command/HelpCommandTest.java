package com.todoapp.command;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HelpCommandTest {

    HelpCommand command;

    @Before
    public void setUp() throws Exception {
        command = new HelpCommand();
    }


    @Test
    public void populateCommandText() throws Exception {
        command.populateCommandText();
        Assert.assertThat("Test for command line option",
                command.commandText.size(),
                CoreMatchers.is(2));
    }

}