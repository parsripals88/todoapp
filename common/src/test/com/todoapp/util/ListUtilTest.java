package com.todoapp.util;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ListUtilTest {

    @Test
    public void intersectList() throws Exception {
        List<Integer> list = ListUtil.intersectList(Arrays.asList(Integer.valueOf("1"), Integer.valueOf("4")),
                Arrays.asList(Integer.valueOf("3"), Integer.valueOf("4")));
        Assert.assertThat("intersect works", list, CoreMatchers.hasItem(4));
    }

}