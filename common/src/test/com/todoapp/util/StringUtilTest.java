package com.todoapp.util;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class StringUtilTest {


    @Test
    public void splitStringBySpace() throws Exception {
        List<String> list = StringUtil.splitStringBySpace("coding is awesome");
        Assert.assertThat("splitStringbySpace check", list.size(), CoreMatchers.is(3));
    }

    @Test
    public void isEmpty() throws Exception {
        boolean isEmpty = StringUtil.isEmpty(null);
        Assert.assertThat("isEmpty check", isEmpty, CoreMatchers.is(true));
    }

    @Test
    public void emptyString() throws Exception {
        Assert.assertThat("emptyString check", StringUtil.emptyString(), CoreMatchers.is(""));
    }

}