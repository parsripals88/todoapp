
import com.todoapp.command.Command;
import com.todoapp.mapper.CommandMapper;
import com.todoapp.mapper.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class Main {

    static Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * Main program of the application
     *
     * @param args
     */
    public static void main(String[] args) {
        while (true) {
            try {
                Scanner scanner = new Scanner(System.in);
                printCommandLine();
                String input = scanner.nextLine();
                if (input != null && input.length() > 0) {
                    Command command = mapInput(input);
                    if (command != null) {
                        command.execute();
                    }
                }
            } catch (Exception e) {
                logger.error("Command Line Interface Exception", e);
            }
        }
    }

    /**
     * mimic a command line interface
     */
    public static void printCommandLine() {
        System.out.print(">");
    }


    /**
     * call mapper to convert input to command class.
     *
     * @param input
     * @return
     */
    public static Command mapInput(String input) {
        Mapper mapper = new CommandMapper();
        return (Command) mapper.mapStringToObject(input);
    }

}
