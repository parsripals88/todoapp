package com.todoapp.command;

import com.todoapp.writer.ConsoleWriter;
import com.todoapp.writer.Writer;
import com.todoapp.annotation.CommandLineOption;
import com.todoapp.model.Notes;
import com.todoapp.service.DefaultReadService;
import com.todoapp.service.ReadService;
import com.todoapp.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

@CommandLineOption(name = "searchtag", description = "search note based on tag", options = "word|words")
public class SearchTagCommand implements Command {

    List<String> words = new ArrayList<>();
    private ReadService service;
    private Writer writer = new ConsoleWriter();

    public SearchTagCommand(String input) {
        parse(input);
        service = new DefaultReadService();
    }

    public SearchTagCommand(String input, ReadService service) {
        parse(input);
        this.service = service;
    }

    @Override
    public void execute() {
        int wordCount = words.size();
        if (wordCount == 0) {
            System.out.println("Not tag words found");
        } else if (wordCount == 1) {
            Notes notes = service.searchTag(words.get(0));
            writer.write(notes);
        } else {
            Notes notes = service.searchTag(words);
            writer.write(notes);
        }
    }

    private void parse(String input) {
        words = StringUtil.splitStringBySpace(input);
    }
}
