package com.todoapp.command;

import com.todoapp.writer.ConsoleWriter;
import com.todoapp.writer.Writer;
import com.todoapp.annotation.CommandLineOption;
import com.todoapp.data.SelectOption;
import com.todoapp.model.Note;
import com.todoapp.model.Notes;
import com.todoapp.service.DefaultReadService;
import com.todoapp.service.ReadService;
import org.apache.commons.lang3.StringUtils;

@CommandLineOption(name = "list", description = "list node", options = "all|number")
public class ListCommand implements Command {

    private SelectOption option;
    private int number;
    private ReadService service;
    private Writer writer = new ConsoleWriter();

    public ListCommand(String input) {
        parse(input);
        service = new DefaultReadService();
    }

    public ListCommand(String input, ReadService service) {
        parse(input);
        this.service = service;
    }

    @Override
    public void execute() {
        switch (option) {
            case ALL:
                Notes notes = service.listAll();
                writer.write(notes);
                break;
            case SINGLE:
                Note note = service.list(number);
                writer.write(note);
                break;
            case INVALID:
                System.out.println("Invalid option");
        }
    }

    private void parse(String str) {
        if ("all".equalsIgnoreCase(str)) {
            option = SelectOption.ALL;
        } else if (StringUtils.isNumeric(str)) {
            option = SelectOption.SINGLE;
            number = Integer.parseInt(str);
        } else {
            option = SelectOption.INVALID;
        }
    }


}
