package com.todoapp.command;

import com.todoapp.writer.ConsoleWriter;
import com.todoapp.writer.Writer;
import com.todoapp.annotation.CommandLineOption;
import com.todoapp.model.Notes;
import com.todoapp.service.DefaultReadService;
import com.todoapp.service.ReadService;
import com.todoapp.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

@CommandLineOption(name = "searchdesc", description = "search note based on description", options = "word|words")
public class SearchDescCommand implements Command {

    List<String> words = new ArrayList<>();
    private ReadService service;
    private Writer writer = new ConsoleWriter();

    public SearchDescCommand(String input) {
        parse(input);
        service = new DefaultReadService();
    }

    public SearchDescCommand(String input, ReadService service) {
        parse(input);
        this.service = service;
    }

    @Override
    public void execute() {
        int wordCount = words.size();
        if (wordCount == 0) {
            System.out.println("No note found");
        } else if (wordCount == 1) {
            Notes notes = service.searchDescription(words.get(0));
            writer.write(notes);
        } else {
            Notes notes = service.searchDescription(words);
            writer.write(notes);
        }
    }

    private void parse(String input) {
        words = StringUtil.splitStringBySpace(input);
    }


}
