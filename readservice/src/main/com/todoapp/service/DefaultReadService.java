package com.todoapp.service;


import com.todoapp.database.NotesDAO;
import com.todoapp.database.NotesInMemoryDAOImpl;
import com.todoapp.indexer.DescriptionIndexer;
import com.todoapp.indexer.Indexer;
import com.todoapp.indexer.TagIndexer;
import com.todoapp.model.Note;
import com.todoapp.model.Notes;

import java.util.List;

public class DefaultReadService implements ReadService {

    NotesDAO dao;
    Indexer tagIndexer;
    Indexer descriptionIndexer;

    public DefaultReadService(NotesDAO dao, Indexer tagIndexer, Indexer descriptionIndexer) {
        this.dao = dao;
        this.tagIndexer = tagIndexer;
        this.descriptionIndexer = descriptionIndexer;
    }

    public DefaultReadService() {
        this.dao = NotesInMemoryDAOImpl.getInstance();
        this.tagIndexer = TagIndexer.getInstance();
        this.descriptionIndexer = DescriptionIndexer.getInstance();
    }

    public Notes listAll() {
        return dao.listAll();
    }

    private Notes list(List<Integer> ids) {
        Notes notes = new Notes();
        if (ids != null) {
            for (int id : ids) {
                notes.addNote(list(id));
            }
        }
        return notes;
    }

    @Override
    public Note list(int id) {
        return dao.listNote(id);
    }

    @Override
    public Notes searchTag(String word) {
        List<Integer> ids = tagIndexer.search(word);
        return list(ids);
    }

    @Override
    public Notes searchTag(List<String> words) {
        List<Integer> ids = tagIndexer.search(words);
        return list(ids);
    }

    @Override
    public Notes searchDescription(String word) {
        List<Integer> ids = descriptionIndexer.search(word);
        return list(ids);
    }

    @Override
    public Notes searchDescription(List<String> words) {
        List<Integer> ids = descriptionIndexer.search(words);
        return list(ids);
    }

}
