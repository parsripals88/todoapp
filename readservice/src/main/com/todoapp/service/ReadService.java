package com.todoapp.service;

import com.todoapp.model.Note;
import com.todoapp.model.Notes;

import java.util.List;

public interface ReadService {

    public Notes listAll();

    public Note list(int id);

    public Notes searchTag(String word);

    public Notes searchTag(List<String> words);

    public Notes searchDescription(String word);

    public Notes searchDescription(List<String> words);
}
