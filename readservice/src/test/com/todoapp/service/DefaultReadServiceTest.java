package com.todoapp.service;

import com.todoapp.database.NotesDAO;
import com.todoapp.indexer.Indexer;
import com.todoapp.model.Note;
import com.todoapp.model.Notes;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

public class DefaultReadServiceTest {

    ReadService service;

    NotesDAO dao;
    Indexer tagIndexer;
    Indexer descIndexer;

    @Before
    public void setUp() throws Exception {
        dao = Mockito.mock(NotesDAO.class);
        tagIndexer = Mockito.mock(Indexer.class);
        descIndexer = Mockito.mock(Indexer.class);
        service = new DefaultReadService(dao, tagIndexer, descIndexer);
    }

    @Test
    public void listAll() throws Exception {
        Notes notes = new Notes();
        Mockito.when(dao.listAll()).thenReturn(notes);
        service.listAll();
        Mockito.verify(dao, Mockito.atLeastOnce()).listAll();
    }

    @Test
    public void list() throws Exception {
        Note note = new Note();
        Mockito.when(dao.listNote(1)).thenReturn(note);
        service.list(1);
        Mockito.verify(dao, Mockito.atLeastOnce()).listNote(1);
    }

    @Test
    public void searchTag() throws Exception {
        List<Integer> integer = Arrays.asList(Integer.valueOf(1));
        Note note = new Note();
        Mockito.when(tagIndexer.search("word")).thenReturn(integer);
        Mockito.when(dao.listNote(1)).thenReturn(note);
        service.searchTag("word");
        Mockito.verify(tagIndexer, Mockito.atLeastOnce()).search("word");
    }

    @Test
    public void searchTag1() throws Exception {
        List<Integer> integer = Arrays.asList(Integer.valueOf(1));
        Note note = new Note();
        List<String> words = Arrays.asList("word", "word1");
        Mockito.when(tagIndexer.search(words)).thenReturn(integer);
        Mockito.when(dao.listNote(1)).thenReturn(note);
        service.searchTag(words);
        Mockito.verify(tagIndexer, Mockito.atLeastOnce()).search(words);
    }

    @Test
    public void searchDescription() throws Exception {
        List<Integer> integer = Arrays.asList(Integer.valueOf(1));
        Note note = new Note();
        Mockito.when(descIndexer.search("word")).thenReturn(integer);
        Mockito.when(dao.listNote(1)).thenReturn(note);
        service.searchDescription("word");
        Mockito.verify(descIndexer, Mockito.atLeastOnce()).search("word");
    }

    @Test
    public void searchDescription1() throws Exception {
        List<Integer> integer = Arrays.asList(Integer.valueOf(1));
        Note note = new Note();
        List<String> words = Arrays.asList("word", "word1");
        Mockito.when(descIndexer.search(words)).thenReturn(integer);
        Mockito.when(dao.listNote(1)).thenReturn(note);
        service.searchDescription(words);
        Mockito.verify(descIndexer, Mockito.atLeastOnce()).search(words);
    }

}