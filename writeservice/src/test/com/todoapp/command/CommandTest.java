package com.todoapp.command;

import com.todoapp.model.Note;
import com.todoapp.service.DefaultWriteService;
import com.todoapp.service.WriteService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class CommandTest {

    Command command;
    WriteService service;

    @Before
    public void setUp() throws Exception {
        service = Mockito.mock(DefaultWriteService.class);
    }

    @Test
    public void addExecute() throws Exception {
        command = new AddCommand("alpha is great", service);
        command.execute();
        Mockito.verify(service, Mockito.atLeastOnce()).add(Mockito.any());
    }

    @Test
    public void deleteExecute() throws Exception {
        command = new DeleteCommand("all", service);
        command.execute();
        Mockito.verify(service, Mockito.atLeastOnce()).deleteAll();
    }

    @Test
    public void updateExecute() throws Exception {
        command = new UpdateCommand("1 alpha is great", service);
        command.execute();
        Mockito.verify(service, Mockito.atLeastOnce()).update(Mockito.any());
    }

}