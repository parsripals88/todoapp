package com.todoapp.service;

import com.todoapp.database.NotesDAO;
import com.todoapp.model.Note;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DefaultWriteServiceTest {

    WriteService service;

    NotesDAO dao;

    @Before
    public void setUp() throws Exception {
        dao = Mockito.mock(NotesDAO.class);
        service = new DefaultWriteService(dao);
    }

    @Test
    public void add() throws Exception {
        Note note = new Note();
        Mockito.when(dao.addNote(note)).thenReturn(true);
        service.add(note);
        Mockito.verify(dao, Mockito.atLeastOnce()).addNote(note);
    }

    @Test
    public void update() throws Exception {
        Note note = new Note();
        Mockito.when(dao.updateNote(note)).thenReturn(true);
        service.update(note);
        Mockito.verify(dao, Mockito.atLeastOnce()).updateNote(note);
    }

    @Test
    public void delete() throws Exception {
        int id = 1;
        Mockito.when(dao.deleteNote(id)).thenReturn(true);
        service.delete(id);
        Mockito.verify(dao, Mockito.atLeastOnce()).deleteNote(id);
    }

    @Test
    public void deleteAll() throws Exception {
        Mockito.when(dao.deleteAll()).thenReturn(true);
        service.deleteAll();
        Mockito.verify(dao, Mockito.atLeastOnce()).deleteAll();
    }

}